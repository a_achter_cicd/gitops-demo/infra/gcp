# Terraform for Kubernetes Cluster on Google Cloud

## GitOps Demo Group with Terraform Cloud

See [Global Readme file](https://gitlab.com/gitops-demo/readme/-/blob/master/README.md) for the full details.

```bash
├── backend.tf         # State file Location Configuration
├── gke.tf             # Google GKE Configuration
├── gitlab-admin.tf    # Adding kubernetes service account
└── group_cluster.tf   # Registering kubernetes cluster to GitLab `apps` Group
```
