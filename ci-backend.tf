terraform {
  backend "remote" {
    organization = "achterkamp"

    workspaces {
      name = "gitops_gcp"
    }
  }
}

