provider "google" {
  credentials = file("./creds/serviceaccount.json")
  project     = "my-project-43714-gke"
  region      = "europe-west1"
}
